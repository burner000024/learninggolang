package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Hello, 世界")
	//Slice
	var items = []string{}
	fmt.Printf("Type %T\n", items)

	check := make([]int, 0)
	check = append(check, 1, 2, 3, 4, 5)
	check[0] = 99
	check = append(check, 100, 2, 3, 4, 5)
	sort.Ints(check)
	fmt.Println(check)
	//Removing element from slice
	var test = []string{"A", "B", "C", "D"}
	var index int = 2
	test = append(test[:index], test[index+1:]...)
	fmt.Println(test)

	// Map Creation
	lang := make(map[string]string)
	lang["JV"] = "Java"
	lang["PY"] = "Python"
	fmt.Println(lang)
	delete(lang, "PY")
	fmt.Println(lang)

	//Looping the map
	for key, value := range lang {
		fmt.Printf("The key is %v, val is %v\n", key, value)

	}

	//or using comma ok syntax
	for _, value := range lang {
		fmt.Printf("For the key, val is %v\n", value)

	}
	Data := User{"Abhijit Nashi", 22}
	fmt.Println("Details are", Data)
	fmt.Printf("Nashi Details are %+v\n", Data)
	fmt.Printf("Individual Values Name %v,Age %v", Data.Name, Data.Age)
	Data.GetAge()  //Using getters to get Age
	
	//If else
	cnt := 5
	if cnt < 5 {				//or init and check at same time ----->  if cnt:=5; cnt <5{}
		fmt.Println("Less than 5")
	} else if cnt > 5 {
		fmt.Println("Grt than 5")
	} else {
		fmt.Println("Equal to 5")
	}

	//Iteration using for loop

	test1 := []string{"Maths", "Science", "English"}

	for a := 0; a < len(test1); a++ {
		fmt.Println(test1[a])
	}

	for i := range test1 {
		fmt.Println(test1[i])
	}

	for index, testing := range test1 {
		fmt.Printf("Index is %v, Value is %v\n", index, testing)
	}

	for _, testhaibahi := range test1 {
		fmt.Printf("value is %v\n", testhaibahi)
	}

	colors := []string{"red", "yellow", "green"}
	for index, color := range colors {
		fmt.Printf("The index of the %v is %v\n", color, index)
	}

	//Function
	res, msg := adder(2, 3)
	fmt.Println(res)
	fmt.Println(msg)
}
func adder(valo int, valt int) (int, string) {
	return valo + valt, "Hey"
}

type User struct {
	Name string
	Age  int
}

func (u User) GetAge() {                 //Methods to modify the Struct class
	fmt.Println("User Age", u.Age)
}
