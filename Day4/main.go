package main

import "fmt"

func main() {

	//defer
	defer fmt.Println("World")
	check()
	fmt.Println("hello")

}

func check() {
	for i := 0; i < 3; i++ {
		defer fmt.Println(i)
	}
}

//o/p -->2 1 0 hello world 


//Using os to perform file operations
package main

import (
	"fmt"
	"os"
)

func main() {
	// Creating a file
	file, err := os.Create("test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	// Writing to a file
	file.WriteString("Hello, World!")

	// Reading a file
	data, err := os.ReadFile("test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(data))

	// Removing a file
	err = os.Remove("test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
}


//Using io to perform file operations

package main

import (
    "fmt"
    "io"
    "strings"
)

func main() {
    // Creating a reader
    reader := strings.NewReader("Hello, World!")

    // Creating a writer
    var writer strings.Builder

    // Copying data from reader to writer
    _, err := io.Copy(&writer, reader)
    if err != nil {
        fmt.Println(err)
        return
    }

    // Printing the content of the writer
    fmt.Println(writer.String())
}


//Using io and os
package main

import (
    "fmt"
    "io"
    "os"
)

func main() {
    // Open a file
    file, err := os.Open("example.txt")
    if err != nil {
        fmt.Println(err)
        return
    }
    defer file.Close()

    // Create a buffer to read the file content
    buf := make([]byte, 1024)

    // Read the file content using io.Reader
    for {
        n, err := file.Read(buf)
        if err != nil && err != io.EOF {
            fmt.Println(err)
            return
        }
        if n == 0 {
            break
        }
        fmt.Print(string(buf[:n]))
    }
}
