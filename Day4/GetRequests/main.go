//Handling Web requests
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const url := "ANYURL"  //repalce the url with actual url

func main() {
	res, err := http.Get(url)
	errorHandler(err)
	fmt.Printf("%T\n", res)
	defer res.Body.Close()  //always close the opened reponse

	//To read it
	databytes, err := ioutil.ReadAll(res.Body)
	errorHandler(err)
	content := string(databytes) //to readable form
	fmt.Println("The Content", content)


	//Another method to extractresponse using String Builder from byte --> String its used as string builder has many options 
	var responseString strings.Builder  // var var_name d_type
	var bytecount = responseString.Write(databytes)
	fmt.Println("The Content",responseString.String)

}

func errorHandler(err error) {
	if err != nil {
		fmt.Println("Inside error block")
		panic(err)
	}
}
