package main

import (
	"fmt"
	"time"
)

func main() {

	presentDate := time.Now()
	fmt.Println(presentDate)

	extra := presentDate.Format("15-08-2001 Monday")
	fmt.Println(extra)

	num := 90
	var ptr = &num

	fmt.Println("The value is ", *ptr)
	fmt.Println("The address is", ptr)

	//Array
	var arr1 [2]string
	arr1[0] = "one"
	arr1[1] = "five"
	fmt.Println(arr1)
	fmt.Println(len(arr1))

	var arr2 = [3]string{"One", "Two", "Six"}
	fmt.Println(arr2)
	fmt.Println(len(arr2))

	fmt.Println("Slices")
	var slice_1 = []string{"One", "Two"}
	fmt.Println(slice_1)

	slice_1 = append(slice_1, "Three", "Four")
	fmt.Println(slice_1)

	slice_1 = slice_1[2:]
	fmt.Println(slice_1)

}